%{
#include <stdio.h>
#include <stdlib.h>

extern int yylex();
extern int yyparse();
void yyerror(const char* s);
%}

%union {
	char* val;
	float fval;
	Node* node;
}

%token END 0
%token TPLUS TMINUS TMULTIPLY TDIVIDE TCOLON TCOMMA TLCURLY TRCURLY TLPAREN TRPAREN TLBRAKET TRBRAKET TPUSH TTILDE
%token TSEMICOLON TEQUALS TAMPERSAND TCPUSH TDEREFERANCE
%token TIF TWHILE TRETURN
%token TNUM TIDENT TSTR
%type<fval> TNUM
%type<val> TIDENT TSTR TCPUSH

%left TLPAREN TRPAREN
%right TAMPERSAND TDEREFERANCE
%left TCOLON
%left TCOMMA
%left TLCURLY TRCURLY
%left TPLUS TMINUS
%left TMULTIPLY TDIVIDE
%left TPUSH TCPUSH
%left TSEMICOLON

%start library

%%
library:
	functions END
|	%empty;

functions:
	functions function
|	function;

function:
	stream_function
|	static_function;

/*	streamed functions */

stream_function:
	type TIDENT stream_parameters TCOLON stream_statement;

stream_parameters:
	stream_parameters TCOMMA stream_parameter
|	stream_parameter;

stream_parameter:
	type TIDENT
|	%empty;

stream_compound_statement:
	TLCURLY stream_statements TRCURLY;

stream_statements:
	stream_statements stream_statement
|	%empty;

stream_statement:
	stream_compound_statement
|	TIF TLPAREN expression TLPAREN stream_statement
|	TWHILE TLPAREN expression TLPAREN stream_statement
|	TRETURN stream_statement;
|	type TIDENT TEQUALS expression TSEMICOLON
|	type TIDENT TSEMICOLON
|	expression TSEMICOLON
|	TSEMICOLON;

/*	static functions */

static_function:
	type TTILDE TIDENT static_parameters TCOLON static_statement;

static_parameters:
	static_parameters TCOMMA static_parameter
|	static_parameter;

static_parameter:
	type TIDENT
|	%empty;

static_compound_statement:
	TLCURLY static_statements TRCURLY;

static_statements:
	static_statements static_statement
|	%empty;

static_statement:
	static_compound_statement
|	TIF TLPAREN expression TLPAREN static_statement
|	TWHILE TLPAREN expression TLPAREN static_statement
|	TRETURN static_statement;
|	type TIDENT TEQUALS expression TSEMICOLON
|	type TIDENT TSEMICOLON
|	expression TSEMICOLON
|	TSEMICOLON;

/* generic operators */

type:
	subtype
|	subtype TDEREFERANCE;

subtype:
	TIDENT;

expression:
	TLPAREN expression TRPAREN
|	expression TLPAREN function_arguments TRPAREN
|	expression TPLUS expression
|	expression TMINUS expression
|	expression TMULTIPLY expression
|	expression TDIVIDE expression
|	expression TPUSH expression
|	expression TCPUSH expression
|	TAMPERSAND expression
|	TDEREFERANCE expression
|	TNUM
|	TIDENT
|	TSTR;

function_arguments:
	function_arguments TCOMMA expression
|	expression;

%%

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}

