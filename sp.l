%option noyywrap

%{
#include <stdio.h>
#define YY_DECL int yylex()
#include "sp.tab.h"
#define preturn(x) printf("%s\n", #x); return x
%}

%%
[ \t\r\n]							; // ignore all whitespace
(?:[0-9]*\.[0-9]+|[0-9]+\.[0-9]*)f?	{ yylval.fval = (float)atof(yytext); printf("%f ", yylval.fval); preturn(TNUM); }
\"([^\\]*|\\.)*\"					{ yylval.val = strdup(yytext); printf("%s ", yylval.val); preturn(TSTR);}
\~									{ preturn(TTILDE); }
\:									{ preturn(TCOLON); }
\;									{ preturn(TSEMICOLON); }
\,									{ preturn(TCOMMA); }
\+									{ preturn(TPLUS); }
\-									{ preturn(TMINUS); }
\*									{ preturn(TMULTIPLY); }
\/									{ preturn(TDIVIDE); }
\=									{ preturn(TEQUALS); }
\(									{ preturn(TLPAREN); }
\)									{ preturn(TRPAREN); }
\[									{ preturn(TLBRAKET); }
\]									{ preturn(TRBRAKET); }
\{									{ preturn(TLCURLY); }
\}									{ preturn(TRCURLY); }
\&									{ preturn(TAMPERSAND); }
\@                                  { preturn(TDEREFERANCE); }
\-\>								{ preturn(TPUSH); }
"while"								{ preturn(TWHILE); }
"if"								{ preturn(TIF); }
"return"							{ preturn(TRETURN); }
\-\([a-zA-Z_][a-zA-Z0-9_]*\)\>		{ yylval.val = strndup(yytext+2, strlen(yytext)-4); printf("%s ", yylval.val); preturn(TCPUSH); }
[a-zA-Z_][a-zA-Z0-9_]*				{ yylval.val = strdup(yytext); printf("%s ", yylval.val); preturn(TIDENT); }
.									{ printf("Invalid token: %s\n", yytext);  yyterminate(); }
%%