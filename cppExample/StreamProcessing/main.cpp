#include <iostream>
#include "pipeStream.h"

void _stdout(PipeStream<int>* /*int*/ in)
{
	std::thread([=]() {
		int val1 = 0;
		while (in->Read(&val1))
			std::cout << val1 << '\n';
	}).detach();
}

int String2Int(std::string val)
{
	return atoi(val.c_str());
}

int main(int argc, char** argv)
{
	PipeStream<std::string>* args = new PipeStream<std::string>();
	for (int i = 1; i < argc; ++i)
		args->Write(std::string(argv[i]));
	args->Close();
	_stdout((PipeStream<int>*)(new ConvertPipeStream<std::string, int>(*args, String2Int)));

	printf("Press any key to continue...");
	getchar();
}