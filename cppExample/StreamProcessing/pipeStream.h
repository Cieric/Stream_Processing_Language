#pragma once
#include <queue>
#include <mutex>

template<typename From, typename To>
class ConvertPipeStream;

template<typename T>
class PipeStream
{
public:
	std::queue<T> _buffer;
	std::mutex _bufferMutex;
	bool END = false;

	PipeStream() = default;
	PipeStream(const PipeStream<T>& other) : _buffer(other._buffer), END(other.END) {};
	~PipeStream() = default;

	void Write(T val)
	{
		_bufferMutex.lock();
		_buffer.push(val);
		_bufferMutex.unlock();
	}

	virtual bool Read(void* val)
	{
	retry:
		_bufferMutex.lock();
		if (_buffer.size() < 1)
		{
			_bufferMutex.unlock();
			if (END)
				return false;
			goto retry;
		}
		*(T*)val = _buffer.front();
		_buffer.pop();
		_bufferMutex.unlock();
		return true;
	}

	void Close()
	{
		END = true;
	}

	bool Ended()
	{
		return END;
	}
};

template<typename From, typename To>
class ConvertPipeStream : public PipeStream<From>
{
	To (*convert)(From) = nullptr;
public:
	ConvertPipeStream(To(*convert)(From)) : PipeStream<From>(), convert(convert) {};
	ConvertPipeStream(const PipeStream<From>& other, To(*convert)(From)) : PipeStream<From>(other), convert(convert) {};
	ConvertPipeStream(const ConvertPipeStream<From, To>& other) : PipeStream<From>(other), convert(other.convert) {};
	~ConvertPipeStream() = default;

	bool Read(void* val) override
	{
	retry:
		this->_bufferMutex.lock();
		if (this->_buffer.size() < 1)
		{
			this->_bufferMutex.unlock();
			if (this->END)
				return false;
			goto retry;
		}
		*(To*)val = convert(this->_buffer.front());
		this->_buffer.pop();
		this->_bufferMutex.unlock();
		return true;
	}
};